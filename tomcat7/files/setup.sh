#!/bin/bash

# Script configuration
ANT_VERSION=1.8.4
TOMCAT_VERSION=7.0.32
LOG4J_VERSION=1.2.17
SETUP_DIR=~/tomcat-install-tmp

# Install the needed software packages
echo "Installig Packages"
zypper --non-interactive install gcc libapr1-devel libopenssl-devel apache2
sleep 3

# Use the installed JDK
export JAVA_HOME=/usr/java/default/

# Create tomcat user
echo "Creating Tomcat user and group"
groupadd tomcat
useradd -r -G tomcat tomcat
sleep 3

# Install Apache Ant
echo "Installig Ant"
cd /opt
wget -q http://mirror3.layerjet.com/apache//ant/binaries/apache-ant-$ANT_VERSION-bin.tar.gz
tar xfz apache-ant-$ANT_VERSION-bin.tar.gz
rm apache-ant-$ANT_VERSION-bin.tar.gz
ln -s apache-ant-$ANT_VERSION ant 
export ANT_HOME=/opt/ant
export PATH=$ANT_HOME/bin:$PATH
sleep 3

# Download Tomcat & Create Symlink
echo "Installig Tomcat"
cd /opt
wget -q http://apache.mirror.digionline.de/tomcat/tomcat-7/v$TOMCAT_VERSION/bin/apache-tomcat-$TOMCAT_VERSION.tar.gz
tar xfz apache-tomcat-$TOMCAT_VERSION.tar.gz 
rm apache-tomcat-$TOMCAT_VERSION.tar.gz
ln -s apache-tomcat-$TOMCAT_VERSION tomcat7

# Delete the default webapps
rm -r /opt/tomcat7/webapps/*
sleep 3

# Create the Java Service Control Binary
# This is needed to daemonize Tomcat
echo "Building JSVC "
cd /opt/tomcat7/bin
tar xfz commons-daemon-native.tar.gz
cd commons-daemon-1.*-native-src/unix
./configure > configure.log
make > make.log
cp jsvc /opt/tomcat7/bin
sleep 3

# Create the APR Connector for native networking
echo "Building APR"
cd /opt/tomcat7/bin
tar xfz tomcat-native.tar.gz
cd tomcat-native-1.*-src/jni/native
./configure --with-apr=/usr/bin/ > configure.log
make > make.log
cd ..
ant > ant.log
cp -r /opt/tomcat7/bin/tomcat-native-1.*-src/jni/native /opt/tomcat7/bin/
sleep 3

# Configure tomcat to use syslog for logging
# See http://tomcat.apache.org/tomcat-7.0-doc/logging.html
echo "Configuring Tomcat for Syslog Logging"
cd /opt/tomcat7/lib/
wget -q http://apache.lauf-forum.at/tomcat/tomcat-7/v$TOMCAT_VERSION/bin/extras/tomcat-juli-adapters.jar
wget -q http://archive.apache.org/dist/logging/log4j/$LOG4J_VERSION/log4j-$LOG4J_VERSION.jar
cd /opt/tomcat7/bin
rm tomcat-juli.jar
wget -q http://apache.lauf-forum.at/tomcat/tomcat-7/v$TOMCAT_VERSION/bin/extras/tomcat-juli.jar
cp $SETUP_DIR/log4j.properties /opt/tomcat7/lib/
rm /opt/tomcat7/conf/logging.properties
sleep 5

# Set Permissions for Tomcat
echo "Fixing Permissions"
chown -R root:tomcat /opt/tomcat7
chown -R tomcat:tomcat /opt/tomcat7/conf

find /opt/tomcat7 -type d | xargs chmod 750
find /opt/tomcat7 -type f | xargs chmod 640
sleep 5

# Add the filter to syslog ng
echo "Updating Syslog Config"
cat $SETUP_DIR/syslog-ng.conf >> /etc/syslog-ng/syslog-ng.conf
service syslog reload
sleep 3

# Install Tomcat as system service
echo "Installing tomcat as system service"
cp $SETUP_DIR/tomcat7-init.sh /etc/init.d/tomcat7
sleep 2

# Enable Tomcat at system start
echo "Enabling Tomcat Service"
chkconfig tomcat7 on
sleep 2


# Starting Tomcat
echo "Starting Tomcat"
service tomcat7 start
