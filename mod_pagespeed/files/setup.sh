#!/bin/bash

BUILDER_USER=builder

echo "Installing Packages"
zypper --non-interactive install at apache2 python git subversion gperf make gcc python-xml libopenssl-devel libcurl-devel libexpat-devel gettext-runtime gcc-c++
sleep 2

echo "Creating user for building"
useradd -r -m $BUILDER_USER
cp ~/pagespeed-install-tmp/setup_builder.sh /home/builder/setup.sh
su -s /bin/bash $BUILDER_USER -c '~/setup.sh'





  