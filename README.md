SLES 11 Install Scripts
=======================

This project contains some scripts to install the following components on a SLES 11 SP 2 machine.

 * Oracle JDK 7
 * Tomcat 7
 * Googles mod_pagespeed
